﻿using UnityEngine;
using System.Collections;

public class KSResourceManager : Singleton<KSResourceManager> {

	public GameObject LoadPrefab( string prefabName ) 
	{	return (GameObject)Resources.Load (prefabName);
	}

	public void Init () {
		//KSDBManager.Instance;
	}
	
}
