﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KSPoolManager : Singleton<KSPoolManager> {

	// Contains all objects in the game, we use Dictionary for easier managing using Keys
	public Dictionary< string, List<GameObject> >      objectContainer = new Dictionary< string, List<GameObject>>();
	
	// Contains current spawn count of objects, if we reach max spawn count, we reuse old assets, thus despawning them
	public Dictionary< string, int >                   objectCountContainer = new Dictionary< string, int> ();
	
	// Checks and Creates new OObject Containers if it encounters that it is a new prefab Type
	public List<GameObject> CreateContainer( string prefabName ) 
	{
		List<GameObject> tempObjectContainer;
		if (!objectContainer.ContainsKey (prefabName))  
		{	tempObjectContainer = new List<GameObject> ();
			objectContainer.Add ( prefabName, tempObjectContainer );
			objectCountContainer.Add( prefabName, 0 );
			
			return tempObjectContainer;
		}
		
		return objectContainer [prefabName];
	}
	
	
	// Adds a new object to our containers given capacity
	// this will automatically create a new container if it detects a new prefab type
	public void AddNewObject( GameObject prefabObject, uint capacity, string objParentName ) 
	{
		string prefabName = prefabObject.name;
		GameObject objParent = GameObject.Find ( objParentName );
		
		// We instantiate a new container ones a new Prefab Type is instantiated!
		List<GameObject> tempObjectContainer = CreateContainer( prefabName );
		
		GameObject prefab = KSResourceManager.Instance.LoadPrefab ( "Prefabs/" + prefabName );
		if (prefab == null) 
			Debug.LogError (" ObjectMgr : Error Instantiating Prefab, pls fix your prefab object > Name : " + prefabName);
		
		// Create our new objects given capacity and add them to our Dictionary
		GameObject tempObject;
		for ( int i = 0; i < capacity; i++ ) 
		{	tempObject = (GameObject)GameObject.Instantiate( prefab );
			tempObject.gameObject.SetActive(false);
			tempObject.transform.name = tempObject.transform.name + i;
			
			if ( objParent != null )
				tempObject.transform.parent = objParent.transform;
			
			tempObject.transform.position = Vector3.zero;
			tempObjectContainer.Add( tempObject );
		}
	}
	
	// Adds a new object to our containers given capacity
	// this will automatically create a new container if it detects a new prefab type
	public void AddNewObject( string prefabName, uint capacity, GameObject objParent )  
	{
		AddNewObject (prefabName, "", capacity, "", objParent);
	}
	
	// Adds a new object to our containers given capacity
	// this will automatically create a new container if it detects a new prefab type
	public void AddNewObject( string prefabName, uint capacity, string objParentName )  
	{
		AddNewObject (prefabName, "", capacity, objParentName, null);
	}
	
	// Adds a new object to our containers given capacity
	// this will automatically create a new container if it detects a new prefab type
	public void AddNewObject( string prefabName, string folder, uint capacity, string objParentName, GameObject objectParent )  
	{
		GameObject objParent;
		
		if( objectParent )
			objParent = objectParent.gameObject;
		else
			objParent = GameObject.Find ( objParentName );
		
		// We instantiate a new container ones a new Prefab Type is instantiated!
		List<GameObject> tempObjectContainer = CreateContainer( prefabName );
		
		GameObject prefab = KSResourceManager.Instance.LoadPrefab ( "Prefabs/" + folder + prefabName );
		if (prefab == null) 
		{	Debug.LogError (" ObjectMgr : Error Instantiating Prefab, please put your prefab to Resources/Prefabs folder, Name : " + prefabName);
			return;
		}
		
		// Create our new objects given capacity and add them to our Dictionary
		GameObject tempObject;
		for ( int i = 0; i < capacity; i++ ) 
		{	tempObject = (GameObject)GameObject.Instantiate( prefab );
			tempObject.gameObject.SetActive(false);
			tempObject.transform.name = tempObject.transform.name + i;
			Debug.Log ("Create Prefab : " + prefabName);	
			if ( objParent != null )
				tempObject.transform.parent = objParent.transform;
			
			tempObject.transform.position = Vector3.zero;
			tempObjectContainer.Add( tempObject );
		}
	}
	
	//Spawn Object in position, with rotation
	public GameObject Spawn3D ( string prefabname, Vector3 position ) 
	{
		List<GameObject> tempObjectContainer = objectContainer [prefabname];
		
		if ( objectCountContainer [prefabname] >= tempObjectContainer.Count )
			objectCountContainer [prefabname] = 0;
		
		GameObject objectToSpawn = tempObjectContainer [ objectCountContainer [prefabname] ];
		
		objectCountContainer [prefabname]++;
		
		if ( objectToSpawn.GetComponent<Rigidbody2D>() != null )
			objectToSpawn.rigidbody2D.velocity = Vector2.zero;
		
		if( objectToSpawn.GetComponent<Rigidbody>() != null )
			objectToSpawn.rigidbody.velocity = Vector3.zero;
		
		
		objectToSpawn.transform.localPosition = position;
		
		objectToSpawn.gameObject.SetActive (true);
		
		return objectToSpawn;
	}
	
	//Spawn Object in position, with rotation
	public GameObject Spawn2D ( string prefabname, Vector2 position ) 
	{
		List<GameObject> tempObjectContainer = objectContainer [prefabname];
		
		if ( objectCountContainer [prefabname] >= tempObjectContainer.Count )
			objectCountContainer [prefabname] = 0;
		
		GameObject objectToSpawn = tempObjectContainer [ objectCountContainer [prefabname] ];
		
		objectCountContainer [prefabname]++;
		
		if ( objectToSpawn.GetComponent<Rigidbody2D>() != null )
			objectToSpawn.rigidbody2D.velocity = Vector2.zero;
		
		if( objectToSpawn.GetComponent<Rigidbody>() != null )
			objectToSpawn.rigidbody.velocity = Vector3.zero;
		
		
		objectToSpawn.transform.localPosition = position;
		
		objectToSpawn.gameObject.SetActive (true);
		
		return objectToSpawn;
	}
	
	public GameObject ChangeParent( string prefabname, GameObject objParent, bool isEnabled=false ) 
	{
		List<GameObject> tempObjectContainer = objectContainer [prefabname];
		
		if ( objectCountContainer [prefabname] >= tempObjectContainer.Count )
			objectCountContainer [prefabname] = 0;
		
		GameObject objectToSpawn = tempObjectContainer [objectCountContainer [prefabname]];
		
		objectCountContainer [prefabname]++;
		
		if ( objParent != null )
			objectToSpawn.transform.parent = objParent.transform;
		
		if( isEnabled )
		{	objectToSpawn.transform.localPosition = Vector3.zero;
			objectToSpawn.gameObject.SetActive (true);
		}
		
		return objectToSpawn;
	}
	
	public GameObject ChangeParent( string prefabname, string objParentName, bool isEnabled=false ) 
	{
		GameObject objParent = GameObject.Find (objParentName);
		
		if( objectContainer [prefabname] == null )
			Debug.LogWarning ("Prefab Missing , Please add object to list via AddObject, Name : " + prefabname);
		
		List<GameObject> tempObjectContainer = objectContainer [prefabname];
		
		if (objectCountContainer [prefabname] >= tempObjectContainer.Count)
			objectCountContainer [prefabname] = 0;
		
		GameObject objectToSpawn = tempObjectContainer [objectCountContainer [prefabname]];
		
		objectCountContainer [prefabname]++;
		
		if ( objParent != null )
			objectToSpawn.transform.parent = objParent.transform;
		
		if( isEnabled )
		{	objectToSpawn.transform.localPosition = Vector3.zero;
			objectToSpawn.gameObject.SetActive (true);
		}
		
		return objectToSpawn;
	}
	
	public void DestroyAll() 
	{
		foreach ( KeyValuePair< string, List<GameObject> > dictionaryObj in objectContainer )
		{	List<GameObject> tempObjectContainer = dictionaryObj.Value;
			
			foreach( GameObject obj in tempObjectContainer )
			{	GameObject.Destroy( obj );
			}
		}
		
		objectContainer.Clear ();
		objectCountContainer.Clear ();
		
		objectContainer = new Dictionary< string, List<GameObject>>();
		objectCountContainer = new Dictionary< string, int> ();
	}
	
	public void ResetAll() 
	{
		foreach ( KeyValuePair< string, List<GameObject> > dictionaryObj in objectContainer )
		{	List<GameObject> tempObjectContainer = dictionaryObj.Value;
			
			foreach( GameObject obj in tempObjectContainer )
			{	obj.SetActive(false);
			}
		}
	}

	public void Init () {
		//KSDBManager.Instance;
	}
	
}
