﻿using UnityEngine;
using System.Collections;

public class KSCameraManager : Singleton<KSCameraManager> {

	public Camera cameraGame;

	// Use this for initialization
	void Start () {
		cameraGame = GameObject.Find ("Main Camera").GetComponent<Camera>();//GameObject.FindGameObjectWithTag ("GameCamera").GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Init () {
		//KSDBManager.Instance;
	}
	
	public void CameraShake(float magnitude, float duration)
	{
		if( cameraGame )
		{	StartCoroutine(Shake(0.2f,0.5f,cameraGame));
		}
		else
		{	StartCoroutine(Shake(0.2f,0.5f,Camera.main));
		}
	}
	
	public void CameraShake(float magnitude, float duration, Camera cam)
	{
		StartCoroutine(Shake(0.2f,0.5f,cam));
	}
	
	IEnumerator Shake(float magnitude, float duration, Camera cam) 
	{
		float elapsed = 0.0f;
		
		Vector3 originalCamPos = cam.transform.position;
		
		while (elapsed < duration) {
			
			elapsed += Time.deltaTime;          
			
			float percentComplete = elapsed / duration;         
			float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);
			
			// map value to [-1, 1]
			float x = Random.value * 2.0f - 1.0f;
			float y = Random.value * 2.0f - 1.0f;
			x *= magnitude * damper;
			y *= magnitude * damper;
			
			cam.transform.position = new Vector3(originalCamPos.x + x, originalCamPos.y +y, originalCamPos.z);
			
			yield return null;
		}
	}
}
