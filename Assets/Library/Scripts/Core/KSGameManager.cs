﻿using UnityEngine;
using System.Collections;

public class KSGameManager : Singleton<KSGameManager> {

	public int APP_VER;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Awake () {
		DontDestroyOnLoad (this.gameObject);
		Init ();
		Application.targetFrameRate = 60;
	}

	public void Init () {

		KSDBManager.Instance.Init ();
		KSCameraManager.Instance.Init ();
		KSInputManager.Instance.Init ();
		KSPoolManager.Instance.Init ();
		KSAudioManager.Instance.Init ();
		KSResourceManager.Instance.Init ();
		//KSDBManager.Instance;
	}
	
}
