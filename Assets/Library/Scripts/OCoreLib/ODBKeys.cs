﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class ODBKeys
{
	// ATTENTION: ADD ALL DB KEYS HERE!

	public const string APP_VER = "APP_VER";
	public const string IS_APP_FIRST_OPEN = "IS_APP_FIRST_OPEN";

	// REMOTE CONFIG
	public const string REMOTE_CONFIG = "REMOTE_CONFIG";
	public const string REMOTE_DOWNLOAD_DATE = "REMOTE_DOWNLOAD_DATE";

	// ANALYTICS
	public const string ANALYTICS_SEND_ONCE_EVENTS = "ANALYTICS_SEND_ONCE_EVENTS";
	public const string ANALYTICS_OPT_OUT = "ANALYTICS_OPT_OUT";
	
	// RATING
	public const string RATING_DONE = "RATING_DONE";

	// FACEBOOK
	public const string FB_USER_ID = "FB_USER_ID";
	public const string FB_USER_NAME = "FB_USER_NAME";
	public const string FB_FRIENDS_ID = "FB_FRIENDS_ID";
	public const string FB_FRIENDS_NAME = "FB_FRIENDS_NAME";
	public const string FB_ACCESS_TOKEN = "FB_ACCESS_TOKEN";
	public const string FB_TOKEN_EXPIRATION = "FB_TOKEN_EXPIRATION";

	// KPI
	public const string KPI_TOTAL_TIME_SPENT = "KPI_TOTAL_TIME_SPENT";

	// DAILY EVENTS
	public const string DAILY_REWARDS_IS_FIRST_TIME = "DAILY_REWARDS_IS_FIRST_TIME";
	public const string DAILY_REWARDS_PREV_TIME = "DAILY_REWARDS_PREV_TIME";
	
	public const string GAME_BGM_ENABLED = "GAME_BGM_ENABLED";
	public const string GAME_SFX_ENABLED = "GAME_SFX_ENABLED";

	// ESKIMO JUMP GAME
	public const string GAME_BEST_SCORE = "GAME_BEST_SCORE";
	public const string GAME_AVE_SCORE = "GAME_AVE_SCORE";
	public const string GAME_TRIES = "GAME_TRIES";
	public const string GAME_ATTEMPTS_FROM_BEST = "GAME_ATTEMPTS_FROM_BEST";
	public const string GAME_UNLOCKED_CHOSEN = "GAME_UNLOCKED_CHOSEN";
	public const string GAME_UNLOCKED = "GAME_UNLOCKED";
	public const string GAME_CURRENCY = "GAME_CURRENCY";

	// ABSORPTION GAME
	public const string GAME_HIGHSCORE_RANGE = "GAME_HIGHSCORE_RANGE";
	public const string GAME_RANGE_RETRIES = "GAME_RANGE_RETRIES";
	public const string GAME_RANGE_DIFF_MULTIPLIER = "GAME_RANGE_DIFF_MULTIPLIER";
	
	// MINIGAMES
	public const string GAME_COINS = "GAME_COINS";
	public const string GAME_COINS_IN_SLOT = "GAME_COINS_IN_SLOT";
	public const string GAME_ACRONYMS = "GAME_ACRONYMS";

	public const string GAME_DISPLAY_NAME = "DISPLAY_NAME";
	public const string GAME_DEVELOPERS = "DEVELOPERS";
	public const string GAME_SCORE_FORMAT = "SCORE_FORMAT";
	public const string GAME_HIGHSCORE = "HIGHSCORE";
	public const string GAME_FRIENDS_HIGHSCORE = "FRIENDS_HIGHSCORE";
	public const string GAME_CP_COLLECTED = "CP_COLLECTED";
	public const string GAME_CP_SCORE = "CP_SCORE";
	public const string GAME_CP_MASCOT = "CP_MASCOT";
	public const string GAME_MENU_ICON = "MENU_ICON";

	public const string FP_GAME_BEST_SCORE = "FP_GAME_BEST_SCORE";
	public const string HP_GAME_BEST_SCORE = "HP_GAME_BEST_SCORE";
	public const string HP_GAME_CHARACTERS_UNLOCKED = "HP_GAME_CHARACTERS_UNLOCKED";
	public const string HP_GAME_CHARACTERS_SELECTED = "HP_GAME_CHARACTERS_SELECTED";

	//+BJB

	public const string AS_CHOSEN_CHAR = "AS_CHOSEN_CHAR";
	public const string AS_CHAR_UNLOCKED = "AS_CHAR_UNLOCKED";

	public const string GAME_DJ_BEST_SCORE = "GAME_DJ_BEST_SCORE";
	public const string DJ_GAME_CHARACTERS_UNLOCKED = "DJ_GAME_CHARACTERS_UNLOCKED";

	public const string KR_GAME_BEST_SCORE = "KR_GAME_BEST_SCORE";
	public const string KR_GAME_CHARACTERS_UNLOCKED = "KR_GAME_CHARACTERS_UNLOCKED";
	public const string KR_GAME_CHARACTERS_SELECTED = "KR_GAME_CHARACTERS_SELECTED";


	//-BJB


	public static Hashtable getInitDBValues()
	{
		Hashtable dbValues = new Hashtable();

		// ATTENTION: INIT ALL KEYS AND VALUES HERE!
		
		dbValues.Add (IS_APP_FIRST_OPEN, true);
		dbValues.Add (APP_VER, 1);  

		// REMOTE CONFIG
		dbValues.Add(REMOTE_CONFIG, "");
		dbValues.Add(REMOTE_DOWNLOAD_DATE, DateTime.Now);

		// ANALYTICS 
		dbValues.Add(ANALYTICS_SEND_ONCE_EVENTS, "");
		dbValues.Add(ANALYTICS_OPT_OUT, false);

		// RATING
		dbValues.Add(RATING_DONE, false);

		// FACEBOOK
		dbValues.Add(FB_USER_ID, "");
		dbValues.Add(FB_USER_NAME, "");
		dbValues.Add(FB_FRIENDS_ID, "");
		dbValues.Add(FB_FRIENDS_NAME, "");
		dbValues.Add(FB_ACCESS_TOKEN, "");
		dbValues.Add(FB_TOKEN_EXPIRATION, DateTime.Now);

		// KPI 
		dbValues.Add(KPI_TOTAL_TIME_SPENT, 0f);

		dbValues.Add(DAILY_REWARDS_IS_FIRST_TIME, true);
		dbValues.Add(DAILY_REWARDS_PREV_TIME, DateTime.Now);
		
		// GAME
		dbValues.Add(GAME_BGM_ENABLED, true);
		dbValues.Add(GAME_SFX_ENABLED, true);

		// ESKIMO JUMP GAME
		dbValues.Add(GAME_BEST_SCORE, 0);
		dbValues.Add(GAME_AVE_SCORE, 0);
		dbValues.Add(GAME_TRIES, 0);
		dbValues.Add(GAME_ATTEMPTS_FROM_BEST, 0);
		dbValues.Add(GAME_UNLOCKED_CHOSEN, 0);
		dbValues.Add(GAME_CURRENCY, 0);

		// ABSORPTION GAME
		dbValues.Add(GAME_HIGHSCORE, 0);
		dbValues.Add(GAME_HIGHSCORE_RANGE, 0);
		dbValues.Add(GAME_RANGE_RETRIES, 0);
		dbValues.Add(GAME_RANGE_DIFF_MULTIPLIER, 1);

		// FLAG THE PLANET
		dbValues.Add(FP_GAME_BEST_SCORE, 0);
	
		// HARD HAT HOPPER
		dbValues.Add(HP_GAME_BEST_SCORE, 0);
		dbValues.Add(HP_GAME_CHARACTERS_UNLOCKED, "");
		dbValues.Add (HP_GAME_CHARACTERS_SELECTED, 0);

		// MINIGAMES
		dbValues.Add(GAME_COINS, 100);
		dbValues.Add(GAME_COINS_IN_SLOT, 0);

		//+BJB

		dbValues.Add (AS_CHOSEN_CHAR, 0);
		dbValues.Add (AS_CHAR_UNLOCKED, false);
		dbValues.Add (GAME_DJ_BEST_SCORE, 0);
		dbValues.Add (DJ_GAME_CHARACTERS_UNLOCKED, "");

		dbValues.Add (KR_GAME_BEST_SCORE, 0);
		dbValues.Add (KR_GAME_CHARACTERS_SELECTED, 0);
		dbValues.Add (KR_GAME_CHARACTERS_UNLOCKED, "");

		//-BJB

		//new OBGameInfo(string acronym, string displayName, string scoreFormat, int coinsRequired)
		/*
		OBGameInfo[] gameInfo = new OBGameInfo[]{	new OBGameInfo("DC", "Do Not Crash", "F0", "Michelle Chen & Annie Chen", "fox",
				new OBGameCPInfo[]{	new OBGameCPInfo(2, "boy_00"),
				new OBGameCPInfo(4, "boy_00"),
				new OBGameCPInfo(6, "boy_00"),
				new OBGameCPInfo(8, "boy_00"),
				new OBGameCPInfo(10, "boy_00")}),
			
			new OBGameInfo("EJ", "ESKIMO JUMP", "F0", "Derrick Mapagu and Ingrid Tan", "eskimo",
				new OBGameCPInfo[]{	new OBGameCPInfo(2, "boy_00"),
				new OBGameCPInfo(4, "boy_00"),
				new OBGameCPInfo(6, "boy_00"),
				new OBGameCPInfo(8, "boy_00"),
				new OBGameCPInfo(10, "boy_00")}),
			
			new OBGameInfo("AB", "ABSORPTION", "F0", "Michelle Chen & Annie Chen", "fox",
			               new OBGameCPInfo[]{	new OBGameCPInfo(2, "boy_00"),
				new OBGameCPInfo(4, "boy_00"),
				new OBGameCPInfo(6, "boy_00"),
				new OBGameCPInfo(8, "boy_00"),
				new OBGameCPInfo(10, "boy_00")}),
			
			new OBGameInfo("BU", "THESUB", "F0", "Joanna Reyes & MJ Pajaron", "fox",
			    new OBGameCPInfo[]{	new OBGameCPInfo(2, "boy_00"),
				new OBGameCPInfo(4, "boy_00"),
				new OBGameCPInfo(6, "boy_00"),
				new OBGameCPInfo(8, "boy_00"),
				new OBGameCPInfo(10, "boy_00")})
				
				
				};
		*/
		string gameAcronyms = "";
		/*
		for(int i=0; i< gameInfo.Length; i++)
		{	string acronym = gameInfo[i].m_acronym;
			gameAcronyms += acronym+",";
			
			dbValues.Add((acronym + "_" + GAME_DISPLAY_NAME), gameInfo[i].m_displayName);
			dbValues.Add((acronym + "_" + GAME_SCORE_FORMAT), gameInfo[i].m_scoreFormat); 
			dbValues.Add((acronym + "_" + GAME_DEVELOPERS), gameInfo[i].m_developers); 
			dbValues.Add((acronym + "_" + GAME_HIGHSCORE), 0);
			dbValues.Add((acronym + "_" + GAME_FRIENDS_HIGHSCORE), "");
			dbValues.Add((acronym + "_" + GAME_MENU_ICON), gameInfo[i].m_menuIconName);
			
			for(int j=0; j< gameInfo[i].m_checkpoints.Length; j++)
			{	dbValues.Add((acronym + "_" + GAME_CP_COLLECTED + "_" + (j+1)), gameInfo[i].m_checkpoints[j].m_isCollected);
				dbValues.Add((acronym + "_" + GAME_CP_SCORE + "_" + (j+1)), gameInfo[i].m_checkpoints[j].m_score);
				dbValues.Add((acronym + "_" + GAME_CP_MASCOT + "_" + (j+1)), gameInfo[i].m_checkpoints[j].m_mascotSpriteName);
			}	
		}
		*/
		
		dbValues.Add(GAME_ACRONYMS, gameAcronyms);	
		
		return dbValues;
	}

	public static void patchKeys(int dbAppVer)
	{	
		int appVer = OGameMgr.Instance.APP_VER;		
		
		// ATTENTION: PATCH KEYS HERE!

		while(dbAppVer < appVer)
		{
			switch(dbAppVer)
			{
				case 0:	break;
			}

			dbAppVer++;
		}
	}
}
