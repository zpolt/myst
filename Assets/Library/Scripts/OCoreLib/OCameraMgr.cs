﻿using UnityEngine;
using System.Collections;

public class OCameraMgr : Singleton<OCameraMgr> 
{
	public static Camera cameraGUI;
	public static Camera cameraGame;
	public static Camera cameraMain;

	// Find References to our Cameras, and set them up
	public static void SetupCamera () 
	{	cameraMain = Camera.main;
		GameObject cameraGameObj = GameObject.FindGameObjectWithTag ("GameCamera");

		if(cameraMain) 
		{	cameraMain.isOrthoGraphic = true;
			cameraMain.orthographicSize = OGameMgr.Instance.CAMERA_SIZE;
		}

		if(cameraGameObj) 
		{	cameraGame = cameraGameObj.GetComponent<Camera>();
			if(cameraGame)
			{	cameraGame.isOrthoGraphic = true;
				cameraGame.orthographicSize = OGameMgr.Instance.CAMERA_SIZE;
			}
		}
	}

	public void CameraShake(float magnitude, float duration)
	{
		if( cameraGame )
		{	StartCoroutine(Shake(0.2f,0.5f,cameraGame));
		}
		else
		{	StartCoroutine(Shake(0.2f,0.5f,Camera.main));
		}
	}

	public void CameraShake(float magnitude, float duration, Camera cam)
	{
		StartCoroutine(Shake(0.2f,0.5f,cam));
	}

	IEnumerator Shake(float magnitude, float duration, Camera cam) 
	{
		float elapsed = 0.0f;
		
		Vector3 originalCamPos = cam.transform.position;
		
		while (elapsed < duration) {
			
			elapsed += Time.deltaTime;          
			
			float percentComplete = elapsed / duration;         
			float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);
			
			// map value to [-1, 1]
			float x = Random.value * 2.0f - 1.0f;
			float y = Random.value * 2.0f - 1.0f;
			x *= magnitude * damper;
			y *= magnitude * damper;
			
			cam.transform.position = new Vector3(originalCamPos.x + x, originalCamPos.y +y, originalCamPos.z);
			
			yield return null;
		}
	}
}
