﻿//OGameMgr :: Put this in one Game Object, this guy controls everything

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(OEditModeMgr))]
public class OGameMgr : Singleton<OGameMgr> 
{
	private Stack<string>      m_sceneStack = new Stack<string>();
	private OGameScene         m_currentGameScene;
	private OPopupScene        m_currentPopupScene;
	private ONotifScene        m_currentNotifScene;
	private OFader			   m_fader;
	
	private List<OPopupScene>  m_popupList = new List<OPopupScene> ();
	private List<ONotifScene>  m_notifList = new List<ONotifScene> ();
	
	public int APP_VER = 1;
	
	// InMobi
	[HideInInspector] public string INMOBI_TEST_IOS = "f1998450f8f84dc5b2a284989c9f8d84";
	[HideInInspector] public string INMOBI_TEST_ANDROID = "8924d29472fe456fb7abf21bc6f073ba";
	public string INMOBI_IOS = "529d5205d4a14a5094180d30b9246f7a";
	public string INMOBI_ANDROID = "529d5205d4a14a5094180d30b9246f7a";

	// GA
	[HideInInspector] public string GA_TEST_IOS = "UA-50876235-8";
	[HideInInspector] public string GA_TEST_ANDROID = "UA-50876235-8";
	public string GA_IOS = "UA-54007168-9";
	public string GA_ANDROID = "UA-54007168-9";
	
	// TapJoy
	[HideInInspector] public string TAPJOY_TEST_IOS = "423d122c-9a29-4b81-ba4c-1a16612f4af0";
	[HideInInspector] public string TAPJOY_APPSECRET_TEST_IOS = "gmHhe8kbScLA8webCSXp";
	[HideInInspector] public string TAPJOY_TEST_ANDROID = "21dd7938-f069-4831-9277-07dcdf7d7103";
	[HideInInspector] public string TAPJOY_APPSECRET_TEST_ANDROID = "A02R93mGMrjzPC2Rd5pz";
	public string TAPJOY_IOS = "86db5a0c-7777-4e28-8e90-1f9f0727b5d9";
	public string TAPJOY_APPSECRET_IOS = "jWqjSdHdl6TIaAsaIh4r";
	public string TAPJOY_ANDROID = "86db5a0c-7777-4e28-8e90-1f9f0727b5d9";
	public string TAPJOY_APPSECRET_ANDROID = "jWqjSdHdl6TIaAsaIh4r";
	
	public float CAMERA_SIZE {
		get {
			if (Screen.width > Screen.height)
				return 11.36f * Screen.height / Screen.width;
			else
				return 11.36f;
		}
	}
	
	public int PIXELS_TO_UNITS = 100;
	
	public string SPLASH_NEXTSCENE = "TestScenesMenu";

	// Button SFX
	public AudioClip BUTTON_CLICK_SFX;

	public int FONT_SIZE_SMALL = 40;
	public int FONT_SIZE_MEDIUM = 100;
	public int FONT_SIZE_LARGE = 200;

	public Font FONT_PRIMARY;
	public Font FONT_SECONDARY;
	
	public OFader Fader {
		get {
			if (m_fader == null)
			{
				InstantiateFader();
			}
		
			return m_fader;
		}
	}

	float m_sessionStartTime;
	
	void Awake ()  
	{
		Debug.Log ("Game Manager Awake");
		DontDestroyOnLoad (this.gameObject);

		InstantiateIDs();
		InstantiateManagers();
		OCameraMgr.SetupCamera ();

		m_currentPopupScene = null;
		m_sessionStartTime = Time.time;

		Application.targetFrameRate = 60;
	}

	void InstantiateIDs()
	{	
		if(INMOBI_IOS == "")
		{	INMOBI_IOS = INMOBI_TEST_IOS;
			
			#if !ODEBUG
			Debug.LogWarning("OGameMgr :: Please Assign INMOBI_IOS");
			#endif
		}

		if(INMOBI_ANDROID == "")
		{	INMOBI_ANDROID = INMOBI_TEST_ANDROID;
			
			#if !ODEBUG
			Debug.LogWarning("OGameMgr :: Please Assign INMOBI_ANDROID");
			#endif
		}
		
		if(GA_IOS == "")
		{	GA_IOS = GA_TEST_IOS;
			
			#if !ODEBUG
			Debug.LogWarning("OGameMgr :: Please Assign GA_IOS");
			#endif
		}
		
		if(GA_ANDROID == "")
		{	GA_ANDROID = OGameMgr.Instance.GA_TEST_ANDROID;
			
			#if !ODEBUG
			Debug.LogWarning("OGameMgr :: Please Assign GA_ANDROID");
			#endif
		}
		

		if(TAPJOY_IOS == "")
		{	TAPJOY_IOS = TAPJOY_TEST_IOS;
			
			#if !ODEBUG
			Debug.LogWarning("OGameMgr :: Please Assign TAPJOY_IOS");
			#endif
		}
		
		if(TAPJOY_ANDROID == "")
		{	TAPJOY_ANDROID = TAPJOY_TEST_ANDROID;
			
			#if !ODEBUG
			Debug.LogWarning("OGameMgr :: Please Assign TAPJOY_ANDROID");
			#endif
		}
		
		if(TAPJOY_APPSECRET_IOS == "")
		{	TAPJOY_APPSECRET_IOS = TAPJOY_APPSECRET_TEST_IOS;
			
			#if !ODEBUG
			Debug.LogWarning("OGameMgr :: Please Assign TAPJOY_APPSECRET_IOS");
			#endif
		}
		
		if(TAPJOY_APPSECRET_ANDROID == "")
		{	TAPJOY_APPSECRET_ANDROID = TAPJOY_APPSECRET_TEST_ANDROID;
			
			#if !ODEBUG
			Debug.LogWarning("OGameMgr :: Please Assign TAPJOY_APPSECRET_ANDROID");
			#endif
		}

		#if !ODEBUG
		Debug.Log("INMOBI_IOS " + INMOBI_IOS);
		Debug.Log("INMOBI_ANDROID " + INMOBI_ANDROID);
		Debug.Log("GA_IOS " + GA_IOS);
		Debug.Log("GA_ANDROID " + GA_ANDROID);
		Debug.Log("TAPJOY_IOS " + TAPJOY_IOS);
		Debug.Log("TAPJOY_APPSECRET_IOS " + TAPJOY_APPSECRET_IOS);
		Debug.Log("TAPJOY_ANDROID " + TAPJOY_ANDROID);
		Debug.Log("TAPJOY_APPSECRET_ANDROID " + TAPJOY_APPSECRET_TEST_ANDROID);
		#endif
	}

	void InstantiateManagers()
	{
		ODBMgr.Instance.InitMgr();
		OGAHandler.Instance.InitMgr();	
		OResourceMgr.Instance.enabled = true;
		OTapJoyHandler.Instance.InitMgr();
		OInputMgr.Instance.enabled = true;
		OAudioMgr.Instance.InitMgr();
		//OInMobiHandler.Instance.InitMgr();
		OAlertHandler.Instance.InitMgr();
		OShareHandler.Instance.InitMgr();
		OWebViewHandler.Instance.InitMgr();
		OAppStoreHandler.Instance.InitMgr();
	}
	
	void InstantiateFader()
	{
		m_fader = OFader.InstantiateFader("~OFader");
		m_fader.gameObject.transform.parent = Camera.main.transform;
	}

	void Update()
	{	
		OGameScene currGameScene = GetCurrentGameScene();
		
		if(currGameScene != null)
		{	if(currGameScene.m_isAcceptTouch)
			{	
				for(int i=0; i < OInputMgr.Instance.getTouchCount(); i++)
				{	if ( OInputMgr.Instance.isTouchDown(i) ) 
					{	if(currGameScene)
						{	currGameScene.OnTouchDown(OInputMgr.Instance.getTouchDownPos(i));
						}
					}
					
					if ( OInputMgr.Instance.isTouchUp(i) ) 
					{	if(currGameScene)
						{	currGameScene.OnTouchUp(OInputMgr.Instance.getTouchUpPos(i));
						}
					}
				}

				if(!Application.isEditor)
				{
					#if UNITY_ANDROID
					if (Input.GetKeyDown(KeyCode.Escape))
					{	if(currGameScene)
						{	currGameScene.OnKeycodeEscape();
						}
					}
					#endif
				}
			}
		}
	}

	public void AddPopup(OPopupScene popup)
	{
		if (m_popupList.Contains (popup))
			return;

		this.m_popupList.Add (popup);
	}

	public void AddNotif(ONotifScene notif)
	{
		if (m_notifList.Contains (notif))
			return;
		
		this.m_notifList.Add (notif);
	}
	
	public void SetCurrentGameScene( OGameScene gamescene )
	{
		if (m_sceneStack.Count < 1)
		{
			Debug.Log("OGameMgr :: Initial GameScene: " + gamescene.name);
			m_sceneStack.Push (gamescene.name);
		} else {
			if(m_currentGameScene != null)
			{	Debug.Log("OGameMgr :: Changing GameScene: " + m_currentGameScene.name + " -> " + gamescene.name);
			}
		}

		m_currentGameScene = gamescene;
	}

	public OGameScene GetCurrentGameScene()
	{
		if(m_currentNotifScene)
		{	return m_currentNotifScene;
		}
		else if (m_currentPopupScene)
		{	return m_currentPopupScene;
		}
		else
		{	return m_currentGameScene;
		}
	}

	public void ShowPopup(string sceneName)
	{
		foreach(OPopupScene scene in m_popupList)
		{
			if(scene.gameObject.name == sceneName)
			{	
				OGameMgr.Instance.Log("OGameMgr :: ShowPopup " + sceneName);
				
				m_currentPopupScene = scene;
				m_currentPopupScene.gameObject.SetActive(true);
			}	else
			{	scene.gameObject.SetActive(false);
			}
		}
	}

	public void HidePopup()
	{
		m_currentPopupScene.gameObject.SetActive (false);

		m_currentPopupScene = null;
	}
	
	public void ShowNotif(string sceneName)
	{
		foreach(ONotifScene scene in m_notifList)
		{
			if(scene.gameObject.name == sceneName)
			{	
				Debug.Log("OGameMgr :: ShowNotif " + sceneName);
				
				m_currentNotifScene = scene;
				m_currentNotifScene.gameObject.SetActive(true);
			}	else
			{	scene.gameObject.SetActive(false);
			}
		}
	}
	
	public void HideNotif()
	{
		m_currentNotifScene.gameObject.SetActive (false);
		
		m_currentNotifScene = null;
	}
	
	public void PushScene(string sceneName, bool isPopup=false) 
	{
		if (m_sceneStack.Contains (sceneName)) 
		{
			while(m_sceneStack.Peek() != sceneName)
			{
				m_sceneStack.Pop();
			}
		}
		else
		{
			m_sceneStack.Push (sceneName);
		}

		Debug.Log("OGameMgr :: LoadLevel " + sceneName);
		
		Application.LoadLevel (sceneName);

		m_popupList.Clear ();
	}

	public void PopScene() 
	{
		if (m_sceneStack.Count < 1)
			return;

		m_sceneStack.Pop ();

		Application.LoadLevel (m_sceneStack.Peek ());
	}

	public bool IsPopupVisible()
	{
		if(m_currentPopupScene)
			return true;
		return false;
	}

	void TrackTotalPlayTime()
	{
		// Calculate total play time
		float totalSessionTime = (Time.time - m_sessionStartTime) / 60f;
		float totalPlayTime = ODBMgr.Instance.getFloat(ODBKeys.KPI_TOTAL_TIME_SPENT) + totalSessionTime;
		totalPlayTime = Mathf.Round(totalPlayTime * 10f) / 10f;
		ODBMgr.Instance.setFloat(ODBKeys.KPI_TOTAL_TIME_SPENT, totalPlayTime);
		
		// Send Event
		OGAHandler.Instance.SendEvent(OGAKeys.CATEGORY_KPI, OGAKeys.ACTION_APP_FLOW_INTERRUPT, OGAKeys.LABEL_TOTAL_PLAY_TIME, totalPlayTime);
		OGAHandler.Instance.Dispatch();

		if(totalPlayTime >= 1000)
		{
			OGAHandler.Instance.SendEventOnce(OGAKeys.CATEGORY_KPI, OGAKeys.ACTION_APP_FLOW_INTERRUPT, OGAKeys.LABEL_TOTAL_PLAY_TIME_1000, totalPlayTime);
		}

		else if(totalPlayTime >= 500)
		{
			OGAHandler.Instance.SendEventOnce(OGAKeys.CATEGORY_KPI, OGAKeys.ACTION_APP_FLOW_INTERRUPT, OGAKeys.LABEL_TOTAL_PLAY_TIME_500, totalPlayTime);
		}

		else if(totalPlayTime >= 240)
		{
			OGAHandler.Instance.SendEventOnce(OGAKeys.CATEGORY_KPI, OGAKeys.ACTION_APP_FLOW_INTERRUPT, OGAKeys.LABEL_TOTAL_PLAY_TIME_240, totalPlayTime);
		}

		else if(totalPlayTime >= 120)
		{
			OGAHandler.Instance.SendEventOnce(OGAKeys.CATEGORY_KPI, OGAKeys.ACTION_APP_FLOW_INTERRUPT, OGAKeys.LABEL_TOTAL_PLAY_TIME_120, totalPlayTime);
		}

		else if(totalPlayTime >= 60)
		{
			OGAHandler.Instance.SendEventOnce(OGAKeys.CATEGORY_KPI, OGAKeys.ACTION_APP_FLOW_INTERRUPT, OGAKeys.LABEL_TOTAL_PLAY_TIME_60, totalPlayTime);
		}

		else if(totalPlayTime >= 30)
		{
			OGAHandler.Instance.SendEventOnce(OGAKeys.CATEGORY_KPI, OGAKeys.ACTION_APP_FLOW_INTERRUPT, OGAKeys.LABEL_TOTAL_PLAY_TIME_30, totalPlayTime);
		}

		OGAHandler.Instance.Dispatch();

		Debug.Log ("OGameMgr :: Total Play Time : " + totalPlayTime);
	}
	
	public void OnApplicationPause(bool paused)
	{
		Debug.Log("OGameMgr :: OnApplicationPause " + paused);
	
		if (paused)
		{
			Debug.Log("OGameMgr :: CurrentGameScene " + GetCurrentGameScene().gameObject.name + " pauseOnInterrupt: " + GetCurrentGameScene().m_isPauseOnInterrupt);

			if (GetCurrentGameScene().m_isPauseOnInterrupt)
			{
				SetPaused(true);
			}

			TrackTotalPlayTime();
		}
		else
		{
			m_sessionStartTime = Time.time;
		}
	}
	
	public void SetPaused(bool paused)
	{
		Debug.Log("OGameMgr :: " + GetCurrentGameScene().gameObject.name + ".SetScenePaused " + paused);
		GetCurrentGameScene().SetScenePaused(paused);
	}

	public void ClearLists()
	{
		m_notifList.Clear ();
		m_popupList.Clear ();
		OObjectMgr.Instance.DestroyAll ();
	}

	public void Log(string str)
	{
		//Debug.Log (str);
	}

	public void LogWarning(string str)
	{
		//Debug.LogWarning (str);
	}

	public void LogError(string str)
	{
		//Debug.LogError (str);
	}
}
