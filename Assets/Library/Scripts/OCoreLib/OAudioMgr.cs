﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OAudioMgr : Singleton<OAudioMgr> 
{
	AudioSource[] m_audioSources;
	int m_maxAudioCount = 16;
	
	bool m_sfxEnabled = true;
	bool m_bgmEnabled = true;

	public void InitMgr()
	{	
		for (int i=0; i< m_maxAudioCount; i++) 
		{	
			this.gameObject.AddComponent<AudioSource>();
		}
		
		m_audioSources = (AudioSource[]) this.gameObject.GetComponents<AudioSource>();
	}
	
	AudioSource GetAvailableSource() 
	{	
		if(m_audioSources == null)
		{	InitMgr();
		}
		
		for (int i=1; i< m_audioSources.Length; i++) 
		{	
			AudioSource audioSource = (AudioSource) m_audioSources[i];

			if(!audioSource.isPlaying)
			{	
				return audioSource;
			}
		}
		
		return m_audioSources[1];
	}
	
	AudioSource GetAudioSourceForClip(AudioClip audio) 
	{	
		for (int i=0; i< m_audioSources.Length; i++) 
		{	
			AudioSource audioSource = (AudioSource) m_audioSources[i];
			
			if(audioSource.clip == audio)
			{	
				return audioSource;
			}
		}
		
		return null;
	}
	
	public void PlaySFX(AudioClip audio, float pitchvalue)
	{	
		AudioSource audioSource = GetAvailableSource();
		
		if (audioSource != null) 
		{	
			audioSource.clip = audio;
			audioSource.loop = false;
			audioSource.pitch = pitchvalue;
			if( m_sfxEnabled )
				audioSource.Play ();
		}
	}	
	
	public void PlaySFX(AudioClip audio, bool isLooping=false)
	{	
		AudioSource audioSource = GetAvailableSource();
		
		if (audioSource != null) 
		{	
			audioSource.clip = audio;
			audioSource.loop = isLooping;
			audioSource.pitch = 1f;
			if( m_sfxEnabled )
				audioSource.Play ();
		}
	}	
	
	public void PlayBGM(AudioClip audio, bool isLooping=true)
	{	
		if(m_audioSources == null)
		{	InitMgr();
		}
		
		AudioSource audioSource = m_audioSources[0];
		
		if (audioSource != null) 
		{	
			audioSource.clip = audio;
			audioSource.loop = isLooping;
			audioSource.pitch = 1f;
			
			if( m_bgmEnabled )
				audioSource.Play ();
		}
	}

	public void SetBGMPitch(float pitch)
	{
		if(m_audioSources == null)
		{	InitMgr();
		}

		AudioSource audioSource = m_audioSources[0];
		
		if (audioSource != null) 
		{	
			audioSource.pitch = pitch;
		}
	}
	
	public void Pause(AudioClip audio)
	{	
		if( !m_sfxEnabled )
			return;
		
		AudioSource audioSource = GetAudioSourceForClip(audio);
		
		if (audioSource != null)
		{	
			audioSource.Pause ();
		}
	}
	
	public void PauseAll()
	{	
		for (int i=0; i< m_audioSources.Length; i++) 
		{
			AudioSource audioSource = (AudioSource)m_audioSources [i];
			audioSource.Pause();
		}
	}
	
	public void Resume(AudioClip audio)
	{	
		AudioSource audioSource = GetAudioSourceForClip(audio);
		
		if (audioSource != null) 
		{	
			audioSource.Play ();
		}
	}
	
	public void Stop(AudioClip audio)
	{	
		AudioSource audioSource = GetAudioSourceForClip(audio);
		
		if (audioSource != null) 
		{	
			audioSource.Stop ();
		}
	}
	
	public void SetEnabledBGM(bool isEnabled)
	{	
		if(m_audioSources == null)
		{	InitMgr();
		}

		if( isEnabled )
		{
			m_bgmEnabled = true;
			
			AudioSource audioSource = m_audioSources[0];
			audioSource.Play();
		}
		else
		{
			m_bgmEnabled = false;
			
			AudioSource audioSource = m_audioSources[0];
			audioSource.Stop();
		}
	}
	
	public void SetEnabledSFX(bool isEnabled)
	{	
		if( isEnabled )
		{
			m_sfxEnabled = true;
		}
		else
		{
			m_sfxEnabled = false;
			
			for (int i=1; i< m_audioSources.Length; i++) 
			{
				AudioSource audioSource = (AudioSource)m_audioSources [i];
				audioSource.Stop();
			}
		}
	}
	
	public void SetEnabledAll(bool isEnabled)
	{	
		SetEnabledSFX(isEnabled);
		SetEnabledBGM(isEnabled);
	}
}
