﻿using UnityEngine;
using System.Collections;

public class OResourceMgr : Singleton<OResourceMgr> 
{
	private Hashtable m_spritesCache;

	public AudioClip LoadAudioClip( string clipName ) 
	{	return (AudioClip)Resources.Load (clipName) as AudioClip;
	}

	public GameObject LoadPrefab( string prefabName ) 
	{	return (GameObject)Resources.Load (prefabName);
	}

	public GameObject LoadSpriteObject( string spriteName ) 
	{	
		GameObject obj = new GameObject();
		obj.name = spriteName;
		obj.AddComponent<SpriteRenderer>();
		obj.GetComponent<SpriteRenderer>().sprite = (Sprite) Resources.Load<Sprite>(spriteName);
		return obj;
	}

	public Sprite LoadSprite( string spriteName ) 
	{	return (Sprite) Resources.Load<Sprite>(spriteName);
	}
	
	// ATTENTION: spritsheet needs to be initialized first and foremost, initSpritesheet only accepts Multiple sprites
	public void initSpritesheet(string spritesheetName) 
	{		
		if (m_spritesCache == null) 
		{	m_spritesCache = new Hashtable();		
		}
		
		Sprite[] spritesheet = Resources.LoadAll<Sprite>(spritesheetName);
		
		foreach (Sprite sprite in spritesheet) 
		{	
			// sprites with same names are ignored
			if(!m_spritesCache.Contains(sprite.name))
			{	m_spritesCache.Add(sprite.name, sprite);
			}
		}
	}
	
	// ATTENTION: always clear spritesheet cache when changing Scenes
	public void clearSpritesheetCache()
	{	m_spritesCache.Clear();
	}	

	Sprite getSprite(string name) 
	{	if (m_spritesCache.Contains (name)) 
		{	return (Sprite) m_spritesCache[name];		
		}
		
		return null;
	}

	public GameObject LoadSpriteFromSpritesheet( string spriteName ) 
	{	
		GameObject obj = new GameObject();
		obj.name = spriteName;
		obj.AddComponent<SpriteRenderer>();
		obj.GetComponent<SpriteRenderer>().sprite = getSprite(spriteName);
		return obj;
	}
	
}
