﻿/*
 * OInputMgr.cs
 * - Platform Agnostic Input Manager
 * 
 *   DAM
 */ 

using UnityEngine;
using System.Collections;

public class OInputMgr : Singleton<OInputMgr> 
{
	public int getTouchCount()
	{	
		if (!Application.isEditor )  
		{	return Input.touchCount;
		}	else
		{	if( Input.GetMouseButton(0) || Input.GetMouseButtonUp(0) || Input.GetMouseButtonDown(0)) 
			{   return 1;
			}
		}

		return 0;
	}

	public bool isTouch(int idx)
	{
		if( !Application.isEditor )
		{	if(Input.touchCount > idx)
			{	if( Input.GetTouch(idx).phase != TouchPhase.Canceled ) 
				{	return true;
				}
			}
		}
		else
		{	if( Input.GetMouseButton(0) )
			{	return true;
			}
		}
		
		return false;
	}
		
	public bool isTouch() 
	{
		if (!Application.isEditor )  
		{   
			foreach(Touch touch in Input.touches)
			{	if(touch.phase != TouchPhase.Canceled)
				{	return true;
				}
			}
		} 
		else 
		{   if( Input.GetMouseButton(0) ) 
			{   return true;
			}
		}
		
		return false;
	}

	public bool isTouchDown(int idx)
	{
		if( !Application.isEditor )
		{	
			if(Input.touchCount > idx)
			{	if(Input.touches[idx].phase == TouchPhase.Began)
				{	return true;
				}
			}
		}
		else
		{	if( Input.GetMouseButtonDown(0) )
			{	return true;
			}
		}

		return false;
	}

	public bool isTouchDown() 
	{
		if (!Application.isEditor )  
		{   foreach(Touch touch in Input.touches)
			{	if(touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
				{	return true;
				}
			}
		} 
		else 
		{   if( Input.GetMouseButtonDown(0) ) 
			{   return true;
			}
		}
		
		return false;
	}

	public bool isTouchUp(int idx)
	{
		if (!Application.isEditor )  
		{   
			if(Input.touchCount > idx)
			{	if(Input.touches[idx].phase == TouchPhase.Ended)
				{	return true;
				}
			}
		} 
		else
		{	if( Input.GetMouseButtonUp(0) )
			{	return true;
			}
		}
		
		return false;
	}
	
	public bool isTouchUp() 
	{
		if (!Application.isEditor )  
		{   
			foreach(Touch touch in Input.touches)
			{	if(touch.phase == TouchPhase.Ended)
				{	return true;
				}
			}
		} 
		else 
		{   if( Input.GetMouseButtonUp(0) ) 
			{   return true;
			}
		}
		
		return false;
	}
	
	#region GET TOUCH POSITION IN WORLD POINT
	public Vector3 getTouchPos() 
	{
		return getTouchPos(0);
	}
	
	public Vector3 getTouchPos(int index) 
	{
		Vector2 touchInScreenPoint = getTouchScreenPos( index );
		
		return Camera.main.ScreenToWorldPoint ( new Vector3(touchInScreenPoint.x, touchInScreenPoint.y, 0) );
	}
	
	public Vector3 getTouchDownPos() 
	{
		return getTouchDownPos(0);
	}
	
	public Vector3 getTouchDownPos(int index)
	{
		Vector2 touchInScreenPoint = getTouchDownScreenPos(index);

		return Camera.main.ScreenToWorldPoint ( new Vector3( touchInScreenPoint.x, touchInScreenPoint.y, 0) );
	}
	
	public Vector3 getTouchUpPos() 
	{
		return getTouchUpPos(0);
	}
	
	public Vector3 getTouchUpPos(int index)
	{
		Vector2 touchInScreenPoint = getTouchUpScreenPos(index);
		return Camera.main.ScreenToWorldPoint ( new Vector3( touchInScreenPoint.x, touchInScreenPoint.y, 0) );
	}
	#endregion

	#region GET TOUCH POSITION IN SCREEN POINT
	public Vector2 getTouchScreenPos() 
	{
		return getTouchScreenPos(0);
	}

	public Vector2 getTouchScreenPos(int index) 
	{
		if (!Application.isEditor )  
		{   if (Input.touchCount > 0 )
			{   return Input.GetTouch(index).position;
			}
		} 
		else 
		{   if( Input.GetMouseButton(0) || Input.GetMouseButtonUp(0) || Input.GetMouseButtonDown(0)) 
			{   return Input.mousePosition;
			}
		}
		
		return Vector2.zero;
	}
	
	public Vector2 getTouchDownScreenPos()  
	{
		return getTouchDownScreenPos(0);
	}
	
	public Vector2 getTouchDownScreenPos(int index) 
	{
		if (!Application.isEditor) 
		{   if (Input.touchCount > 0 && Input.GetTouch(index).phase == TouchPhase.Began ) 
			{
				return Input.GetTouch(index).position;
			}
			
		} 
		else 
		{   if( Input.GetMouseButtonDown(0) ) 
			{
				return Input.mousePosition;
			}
		}
		
		return Vector2.zero;
	}

	public Vector2 getTouchUpScreenPos()  
	{
		return getTouchUpScreenPos(0);
	}
	
	public Vector2 getTouchUpScreenPos(int index) 
	{
		if (!Application.isEditor) 
		{   if (Input.touchCount > 0 && Input.GetTouch(index).phase == TouchPhase.Ended ) 
			{
				return Input.GetTouch(index).position;
			}
			
		} 
		else 
		{   if( Input.GetMouseButtonUp(0)) 
			{
				return Input.mousePosition;
			}
		}
		
		return Vector2.zero;
	}
	#endregion	

	public bool isSpriteTouch(GameObject sprite) 
	{	return isSpriteTouch(sprite, 0);
	}
	
	public bool isSpriteTouch(GameObject sprite, int idx) 
	{	
		// inactive sprites do not receive touch
		if(!sprite.activeInHierarchy)
		{	Debug.Log(sprite.name + "is inactive");
			return false;
		}
			
		else
		{	if(isSpriteTouchAtPos(getTouchPos(idx), sprite))
			{	return true;
			}
		}				
		
		return false;
	}
					
	public bool isSpriteTouchAtPos(Vector3 pos, GameObject sprite) 
	{	
		if(sprite.collider2D != null)
		{	return sprite.collider2D.OverlapPoint(pos);
		}
		else
		{	if(sprite.renderer.bounds.Contains(new Vector3(pos.x, pos.y, sprite.transform.position.z)))
			{	return true;
			}	else
			{	return false;
			}
		}
		
		return false;
	}
	
	public bool isShaked()
	{	float shakeThreshold = 5;
		if(Input.acceleration.sqrMagnitude > 5)
		{	return true;
		}
		
		return false;
	}
	
	public bool isPortrait()
	{	if(Input.deviceOrientation == DeviceOrientation.Portrait)
		{	return true;
		}
		
		return false;
	}
	
	public bool isPortraitUpsideDown()
	{	if(Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
		{	return true;
		}
		
		return false;
	}
	
	public bool isLandscapeLeft()
	{	if(Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
		{	return true;
		}
		
		return false;
	}
	
	public bool isLandscapeRight()
	{	if(Input.deviceOrientation == DeviceOrientation.LandscapeRight)
		{	return true;
		}
		
		return false;
	}
	
	public bool isFaceUp()
	{	if(Input.deviceOrientation == DeviceOrientation.FaceUp)
		{	return true;
		}
		
		return false;
	}
	
	public bool isFaceDown()
	{	if(Input.deviceOrientation == DeviceOrientation.FaceDown)
		{	return true;
		}
		
		return false;
	}
									
}
