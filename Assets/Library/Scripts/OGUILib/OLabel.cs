﻿using UnityEngine;
using System.Collections;


public enum LABEL_FONT
{
	PRIMARY,
	SECONDARY
}

public enum LABEL_SIZE
{
	SMALL,
	MEDIUM,
	LARGE
}

public enum SORTING_LAYER
{
	DEFAULT,
	UI,
	POPUP,
	NOTIFICATION,
}

[RequireComponent (typeof (MeshRenderer))]
[RequireComponent (typeof (TextMesh))]
[ExecuteInEditMode]
public class OLabel : MonoBehaviour {
	
	public string                 m_text = "Label";
	
	public LABEL_FONT             m_fontType = LABEL_FONT.PRIMARY;
	public LABEL_SIZE             m_fontSize = LABEL_SIZE.MEDIUM;
	public float                  m_fontSizeMultiplier = 1.0f;
	public SORTING_LAYER    	  m_fontSortingLayer = SORTING_LAYER.UI;
	public int    	  			  m_fontSortingOrder = 0;
	
	public TextAlignment 		  m_textAlignment = TextAlignment.Center;
	public TextAnchor			  m_textAnchor = TextAnchor.MiddleCenter;
	public FontStyle			  m_fontStyle = FontStyle.Normal;
	
	public Color 				  m_textColor = Color.white;	
	
	public GameObject             m_shadow;
	
	float                m_fade = 0;
	float				 m_fadeRate = 0;
	float				 m_fadeCounter = 0;
	
	Color      	         m_defaultColor;
	public TextMesh      m_textMesh;
	
	// Use this for initialization
	void Awake () {
		Init ();
	}
	
	void Init()
	{
		m_textMesh = this.GetComponent<TextMesh> ();
		m_textMesh.hideFlags = HideFlags.HideInInspector;
		this.GetComponent<MeshRenderer>().hideFlags = HideFlags.HideInInspector;
		
		m_defaultColor = m_textMesh.color;
		
		this.transform.localScale = new Vector3 (0.05f * m_fontSizeMultiplier, 0.05f * m_fontSizeMultiplier, 0.05f * m_fontSizeMultiplier);
		
		this.UpdateLabel (m_text);
		
		m_textMesh.alignment = m_textAlignment;
		m_textMesh.anchor = m_textAnchor;
		m_textMesh.fontStyle = m_fontStyle;
		m_textMesh.color = m_textColor;
		
		if(OGameMgr.Instance != null)
		{	
			switch(m_fontType)
			{
			case LABEL_FONT.PRIMARY :
				if(OGameMgr.Instance.FONT_PRIMARY != null)
				{	this.m_textMesh.font = OGameMgr.Instance.FONT_PRIMARY;
				}
				break;
			case LABEL_FONT.SECONDARY :
				if(OGameMgr.Instance.FONT_SECONDARY != null)
				{	this.m_textMesh.font = OGameMgr.Instance.FONT_SECONDARY;
				}
				break;
			}
			
			this.m_textMesh.renderer.material = this.m_textMesh.font.material;
			
			switch(m_fontSize)
			{
			case LABEL_SIZE.SMALL :
				if(OGameMgr.Instance.FONT_SIZE_SMALL != null)
				{	this.m_textMesh.fontSize = OGameMgr.Instance.FONT_SIZE_SMALL;
				}
				break;
			case LABEL_SIZE.MEDIUM :
				if(OGameMgr.Instance.FONT_SIZE_MEDIUM != null)
				{	this.m_textMesh.fontSize = OGameMgr.Instance.FONT_SIZE_MEDIUM;
				}
				break;
			case LABEL_SIZE.LARGE :
				if(OGameMgr.Instance.FONT_SIZE_LARGE != null)
				{	this.m_textMesh.fontSize = OGameMgr.Instance.FONT_SIZE_LARGE;
				}
				break;
			}
		}
		
		switch(m_fontSortingLayer)
		{
		case SORTING_LAYER.DEFAULT :
			this.renderer.sortingLayerName = "Default";
			break;
		case SORTING_LAYER.UI :
			this.renderer.sortingLayerName = "UI";
			break;
		case SORTING_LAYER.POPUP :
			this.renderer.sortingLayerName = "Popup";
			break;
		case SORTING_LAYER.NOTIFICATION :
			this.renderer.sortingLayerName = "Notification";
			break;
		}
		
		this.renderer.sortingOrder = m_fontSortingOrder;
		if(m_shadow != null)
		{	m_shadow.renderer.sortingOrder = this.renderer.sortingOrder-1;
		}
		
	}
	
	public void UpdateLabel(int text)
	{	UpdateLabel(text.ToString());
	}
	
	public void UpdateLabel(float text)
	{	UpdateLabel(text.ToString());
	}
	
	public void UpdateLabel(bool text)
	{	UpdateLabel(text.ToString());
	}
	
	public void UpdateLabel(string text)
	{	
		m_text = text;
		
		TextMesh labelText = (TextMesh)this.GetComponent<TextMesh>();
		
		if (labelText != null) 
		{	labelText.text = text;		
		}
		
		if (m_shadow != null) 
		{	TextMesh shadowText = (TextMesh)m_shadow.GetComponent<TextMesh>();
			if (shadowText != null) 
			{	shadowText.text = text;		
			}
		}
	}

	public void AnimateFade(float duration, float rate)
	{
		m_fade = duration;
		m_fadeRate = rate;
		m_fadeCounter = 1f;
		this.m_textMesh.color = new Color(m_defaultColor.r,m_defaultColor.g,m_defaultColor.b,m_fadeCounter);
	}
	
	public virtual void FixedUpdate()
	{
		if(m_fade > 0)
		{
			m_fade -= Time.deltaTime;
			
			if(m_fade < 0)
			{
				this.m_textMesh.color = new Color(m_defaultColor.r,m_defaultColor.g,m_defaultColor.b,0.0f);
			}
			else if(m_fade <= 1.5f)
			{
				m_fadeCounter = m_fadeCounter - 0.05f;
				this.m_textMesh.color = new Color(m_defaultColor.r,m_defaultColor.g,m_defaultColor.b,m_fadeCounter);
			}
		}
	}

	public void Update() 
	{	if (Application.isEditor && !Application.isPlaying)
		{	Init();
		}
	}
}
