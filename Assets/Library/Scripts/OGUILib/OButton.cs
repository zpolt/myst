﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(BoxCollider2D))]
[ExecuteInEditMode]
public class OButton : MonoBehaviour 
{
	public SORTING_LAYER    	  m_sortingLayer = SORTING_LAYER.DEFAULT;
	public int    	  			  m_sortingOrder = 0;

	[HideInInspector]
	public string m_buttonId = "";
	public string m_preferredButtonId = "";

#if UNITY_ANDROID
	public bool isAndroidBackButton = false;
#else 
	[HideInInspector]
	public bool isAndroidBackButton = false;
#endif // UNITY_ANDROID

	public bool isDisabled = false;
	
	[HideInInspector]
	public Color m_buttonColor;
	
	public AudioClip m_soundFX;
	
	public virtual void Awake()
	{
		if (m_preferredButtonId == "")
			m_buttonId = gameObject.name;
		else
			m_buttonId = m_preferredButtonId;
		
		if(m_soundFX == null)
		{	if(OGameMgr.Instance.BUTTON_CLICK_SFX != null)
			{	m_soundFX = OGameMgr.Instance.BUTTON_CLICK_SFX;
			}
		}

		Init();
	}

	void Init()
	{
		if(this.renderer != null)
		{	switch(m_sortingLayer)
			{
			case SORTING_LAYER.DEFAULT :
				this.renderer.sortingLayerName = "Default";
				break;
			case SORTING_LAYER.UI :
				this.renderer.sortingLayerName = "UI";
				break;
			case SORTING_LAYER.POPUP :
				this.renderer.sortingLayerName = "Popup";
				break;
			case SORTING_LAYER.NOTIFICATION :
				this.renderer.sortingLayerName = "Notification";
				break;
			}
			
			this.renderer.sortingOrder = m_sortingOrder;
		}
	}

	public void Tint(Color color)
	{
		if(this.renderer != null)
		{	this.GetComponent<SpriteRenderer> ().color = color;
		}
	}
	
	public void MoveFrom(Vector3 direction, float offset, float time)
	{
		Vector3 fromPosition = transform.TransformPoint ( direction * offset ); 
		Vector3 toPosition = transform.position; 
		transform.position = fromPosition; 
		LeanTween.move(gameObject, toPosition, time); 
	}
	
	public void FadeIn(float time)
	{
		LeanTween.value(gameObject, SetAlpha, 0f, 1f, time); 
	}
	
	public void FadeOut(float time)
	{
		LeanTween.value(gameObject, SetAlpha, 1f, 0f, time); 
	}
	
	public void SetAlpha(float val)
	{
		SpriteRenderer spriteRenderer = renderer as SpriteRenderer;
		Color alpha = spriteRenderer.color; 
		alpha.a = val; 
		spriteRenderer.color = alpha;
	}

	public bool isTouched()
	{	
		return OInputMgr.Instance.isSpriteTouch(gameObject);
	}
	
	public virtual void OnDown()
	{	
		if ( m_soundFX != OGameMgr.Instance.BUTTON_CLICK_SFX )
			OAudioMgr.Instance.PlaySFX(m_soundFX);
	}
	
	public virtual void OnUp()
	{
	}
	
	public virtual void Disable(bool isDisabled)
	{	
		this.isDisabled = isDisabled;
	}

	public void Update() 
	{	
		if (Application.isEditor && !Application.isPlaying)
		{	OGameScene buttonRootScene = this.transform.root.GetComponent<OGameScene>();

			if(!buttonRootScene.m_buttons.Contains(this))
			{	buttonRootScene.m_buttons.Add(this);
			}

			Init();
		}
	}
}