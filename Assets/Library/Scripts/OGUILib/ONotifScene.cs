﻿using UnityEngine;
using System.Collections;

public abstract class ONotifScene : OPopupScene 
{
	public override void Awake () 
	{	this.gameObject.SetActive (false);
		
		OGameMgr.Instance.AddNotif (this);

		m_sortingLayer = SORTING_LAYER.NOTIFICATION;
		setSortingLayer(transform);
		
		//m_bannerAdPlacement = OBannerAdPlacement.None;
		
		InstantiateBackgroundShade();
	}
}
