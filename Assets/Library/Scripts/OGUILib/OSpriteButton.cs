﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(SpriteRenderer))]
public class OSpriteButton : OButton 
{
	public Sprite m_buttonNormalSprite; // optional
	public Sprite m_buttonDownSprite; // optional
	public Sprite m_buttonDisabledSprite; // optional
	
	public override void Awake()
	{
		base.Awake ();

		m_buttonColor = gameObject.GetComponent<SpriteRenderer>().color;
		this.GetComponent<SpriteRenderer>().hideFlags = HideFlags.HideInInspector;
		
		if(m_buttonNormalSprite == null)
		{	m_buttonNormalSprite = gameObject.GetComponent<SpriteRenderer>().sprite;
		}

		Disable(isDisabled);
	}

	public override void OnDown()
	{	
		if (isDisabled)
			return;

		base.OnDown();	
		
		if(m_buttonDownSprite != null)
		{	gameObject.GetComponent<SpriteRenderer>().sprite = m_buttonDownSprite;
		}	else
		{	gameObject.GetComponent<SpriteRenderer>().color = new Color( 0.7f, 0.7f, 0.7f);
		}
	}
	
	public override void OnUp()
	{	
		if (isDisabled)
			return;

		base.OnUp();	
		
		if(m_buttonDownSprite != null)
		{	gameObject.GetComponent<SpriteRenderer>().sprite = m_buttonNormalSprite;
		}	else
		{	gameObject.GetComponent<SpriteRenderer>().color = m_buttonColor;
		}	
	}

	public override void Disable(bool isDisabled)
	{	base.Disable(isDisabled);

		if(isDisabled)
		{	if(m_buttonDisabledSprite != null)
			{	gameObject.GetComponent<SpriteRenderer>().sprite = m_buttonDisabledSprite;
			}	else
			{	gameObject.GetComponent<SpriteRenderer>().color = new Color( 0.7f, 0.7f, 0.7f);
			}
		}	else
		{	if(m_buttonDisabledSprite != null)
			{	gameObject.GetComponent<SpriteRenderer>().sprite = m_buttonNormalSprite;
			}	else
			{	gameObject.GetComponent<SpriteRenderer>().color = m_buttonColor;
			}
		}
	}
}