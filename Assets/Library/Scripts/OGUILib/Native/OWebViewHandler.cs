using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class OWebViewHandler : Singleton<OWebViewHandler> 
{
	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _OpenURL(string url);
	#endif
	
	#if UNITY_ANDROID
	private static AndroidJavaObject jo;
	#endif

	// ONLY WORKS IN IOS
	public delegate void Action();
	public static Action OnWebViewClosed = delegate {};

	bool isOpen = false;
	
	public void InitMgr()
	{
		if(!Application.isEditor)
		{
			#if UNITY_ANDROID
			jo = new AndroidJavaObject("com.orangenose.nativeandroid.WebView");
			#endif
				
		}	else
		{	Debug.Log("OWebViewHandler:: Cannot open WebView in Editor.");
		}
	}

	public void openURL(string url)
	{
		isOpen = true;

		if(!Application.isEditor)
		{
			#if UNITY_IPHONE
			_OpenURL(url);
			#endif

			#if UNITY_ANDROID
			jo.Call ("OpenURL", url);
			#endif
		}	else
		{	Debug.Log("OWebViewHandler:: Cannot open WebView in Editor.");
		}
	}

	void webViewClosed()
	{	
		#if UNITY_IPHONE
		Debug.Log("OWebViewHandler:: WebView closed.");
		isOpen = false;
		OnWebViewClosed();
		#endif
	}

	void OnApplicationPause(bool pauseStatus)
	{	
		#if UNITY_ANDROID
		if(!pauseStatus && isOpen)
		{	Debug.Log("OWebViewHandler:: WebView closed.");
			isOpen = false;
			OnWebViewClosed();
		}
		#endif
	}
}	
