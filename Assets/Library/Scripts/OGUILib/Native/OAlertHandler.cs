﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class OAlertHandler : Singleton<OAlertHandler> 
{
	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _CreateAlert(string title, string msg, string cancelButtonTitle, string otherButtonTitle);
	[DllImport ("__Internal")]
	private static extern void _CreateAlertOK(string title, string msg, string cancelButtonTitle);
	#endif
	
	#if UNITY_ANDROID
	private static AndroidJavaObject jo;
	#endif

	public delegate void Action();
	public static Action OnAlertDismissed = delegate {}; // if selected other button
	public static Action OnAlertCanceled = delegate {}; // if canceled
	
	public void InitMgr()
	{	
		if (!Application.isEditor)
		{	
			#if UNITY_ANDROID
			jo = new AndroidJavaObject("com.orangenose.nativeandroid.Alert");
			#endif
		}	else
		{	Debug.Log("OAlertHandler :: Alerts does not run in Editor.");
		}	
	}
	
	public void createAlert(string title, string msg, string cancelButtonTitle, string otherButtonTitle)
	{	
		if(!Application.isEditor)
		{	Debug.Log("OAlertHandler:: Create Alert " + title);	

			#if UNITY_IPHONE
			_CreateAlert(title, msg, cancelButtonTitle, otherButtonTitle);
			#endif

			#if UNITY_ANDROID
			jo.Call("CreateAlert", title, msg, cancelButtonTitle, otherButtonTitle);
			#endif
		}	else
		{	Debug.Log("OAlertHandler:: Alerts does not run in Editor.");
		}
	}

	public void createAlert(string title, string msg, string otherButtonTitle)
	{	
		if(!Application.isEditor)
		{	Debug.Log("OAlertHandler:: Create Alert " + title);	
			
			#if UNITY_IPHONE
			_CreateAlertOK(title, msg, otherButtonTitle);
			#endif
			
			#if UNITY_ANDROID
			jo.Call("CreateAlert", title, msg, otherButtonTitle);
			#endif
		}	else
		{	Debug.Log("OAlertHandler:: Alerts does not run in Editor.");
		}
	}
	

	void didDismissWithButtonIndex(string button)
	{
		int buttonIndex;

		if(int.TryParse(button, out buttonIndex))
		{
			if(buttonIndex == 0)
			{	OnAlertCanceled();
			}	else
			{	OnAlertDismissed();
			}
		}
	}
}	
