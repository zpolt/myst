﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class OEditModeMgr : MonoBehaviour {

	void Init()
	{
		OCameraMgr.SetupCamera();
	}

	void Update () 
	{
		if (Application.isEditor && !Application.isPlaying)
		{	Init();
		}
	}
}
