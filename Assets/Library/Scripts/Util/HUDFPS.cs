﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (GUIText))]
[ExecuteInEditMode]
public class HUDFPS : MonoBehaviour 
{
	
	// Attach this to a GUIText to make a frames/second indicator.
	//
	// It calculates frames/second over each updateInterval,
	// so the display does not keep changing wildly.
	//
	// It is also fairly accurate at very low FPS counts (<10).
	// We do this not by simply counting frames per interval, but
	// by accumulating FPS for each frame. This way we end up with
	// correct overall FPS even if the interval renders something like
	// 5.5 frames.
	
	public  float updateInterval = 0.1f;
	
	private float accum   = 0; // FPS accumulated over the interval
	private int   frames  = 0; // Frames drawn over the interval
	private float timeleft; // Left time for current interval
	
	/**
	private float firstFPS = 0;	
	private float totalFPS = 0;
	private float aveFPS = 0;
	private float currFPSCount = 0;
	private float currFPSCounter = 0;
	private int nFPS = 3;
	**/
	
	float[] n = new float[]{0, 0, 0};
	int currFPSCount = 0;
	int currFPSCounter = 0;
	int nFPS = 3;
	float totalFPS = 0;
	float aveFPS = 0;
	
	void Start()
	{
		if( !guiText )
		{
			Debug.Log("UtilityFramesPerSecond needs a GUIText component!");
			enabled = false;
		}	else
		{
			guiText.text = "FPS";
			guiText.font = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
			guiText.fontSize = 50;
			guiText.alignment = TextAlignment.Left;
			guiText.anchor = TextAnchor.LowerLeft;
			
			timeleft = updateInterval;  
		}
	}
	
	void Update()
	{
		timeleft -= Time.deltaTime;
		accum += Time.timeScale/Time.deltaTime;
		++frames;
		
		// Interval ended - update GUI text and start new interval
		if( timeleft <= 0.0 )
		{
			// display two fractional digits (f2 format)
			float fps = accum/frames;
			//string format = "CURR FPS: " + fps.ToString("F2");
			//guiText.text = format;
			
			if(fps < 30)
				guiText.material.color = Color.yellow;
			else 
				if(fps < 10)
					guiText.material.color = Color.red;
			else
				guiText.material.color = Color.green;
			
			timeleft = updateInterval;
			accum = 0.0F;
			frames = 0;
			
			n[currFPSCounter] = fps;
			
			totalFPS+=fps;
			currFPSCount++;
			currFPSCounter++;
			
			if(currFPSCount == 3)
			{	aveFPS = totalFPS/nFPS;	

				string format = "FPS " + aveFPS.ToString("F2");
				guiText.text = format;
			}	
			
			if(currFPSCount > 3)
			{	aveFPS = ((aveFPS*3)-n[0]+fps)/nFPS;

				for(int i=0; i< nFPS-1; i++)
				{	n[i] = n[i+1];
				}

				string format = "FPS " + aveFPS.ToString("F2");
				guiText.text = format;
			}

			if(currFPSCounter < nFPS-1)
			{	currFPSCounter++;
			}	else
			{	currFPSCounter = 0;
			}
		}
	}
}