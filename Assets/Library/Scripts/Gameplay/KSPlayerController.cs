﻿using UnityEngine;
using System.Collections;

public class KSPlayerController : TriggerListener {

	[HideInInspector]
	public bool facingRight = true;			// For determining which way the player is currently facing.
	//[HideInInspector]
	public bool jump = false;				// Condition for whether the player should jump.

	public float moveForce = 365f;			// Amount of force added to move the player left and right.
	public float maxSpeed = 5f;				// The fastest the player can travel in the x axis.
	public AudioClip[] jumpClips;			// Array of clips for when the player jumps.
	public float jumpForce = 1000f;			// Amount of force added when the player jumps.
	//[HideInInspector]
	public GameObject groundCheck;
	public GameObject frontCheck;
	private bool grounded = false;

	// player states
	public bool shouldStartGame = false;
	public bool isDying = false;
	public bool canRestart = false;

	public int  nGameScore = 0;
	public int nTotalScore = 10;
	//public tk2dTextMesh labelScore;

	public OLabel labelScore;

	public GameObject[] Coins = {};

	public AudioClip audio_jump;
	public AudioClip audio_die;
	public AudioClip audio_coin;
	
	void Awake()
	{
		// Setting up references.
		//groundCheck = transform.Find("groundCheck");
		/*
		if ( groundCheck == null )
		{
			groundCheck = new GameObject ("groundCheck");
			groundCheck.transform.parent = this.transform;
			groundCheck.transform.localPosition = new Vector2 (0, -0.92f);
		}
		*/
		labelGamesPlayed.UpdateLabel ("Attempts at Love: " + KSDBManager.Instance.getInt (KSDBKeys.MYST_GAMESPLAYED, 0));
		Coins = GameObject.FindGameObjectsWithTag ("Coin");
	}
	
	// Use this for initialization
	void Start () {
		Time.timeScale = 1f;
		//Vector2.right * playerSpeed;

		Scene_Intro_ONCE ();
		Scene_Last_ONCE ();
	}

	private float fInputQueueTimer = 0.0f;
	private float MAX_WAIT = 0.2f;
	private bool bShouldWait = false;


	public GameObject goTaptoPlay;
	public GameObject goHelp;
	public GameObject goScore;

	public GameObject goHelpLast;

	public GameObject goHeartLast;

	public OLabel labelGamesPlayed;


	public void Scene_Intro_ONCE ()
	{
		LeanTween.alpha (goTaptoPlay, 0f, 0.575f).setLoopPingPong ();
		LeanTween.moveY (goHelp, goHelp.transform.position.y + 0.1f, 0.35f).setLoopPingPong ();
	}

	public void Scene_Last_ONCE ()
	{
		//LeanTween.alpha (goTaptoPlay, 0f, 0.5f).setLoopPingPong ();
		LeanTween.moveY (goHelpLast, goHelpLast.transform.position.y + 0.1f, 0.35f).setLoopPingPong ();
		LeanTween.moveY (goHeartLast, goHeartLast.transform.position.y + 0.05f, 1.05f).setLoopPingPong ();
	}

	public void Scene_Replay_AGAIN()
	{
		//goTaptoPlay.SetActive (false);
	}

	public void Scene_Win ( bool didWin )
	{
		if ( didWin )
			goHelpLast.SetActive (false);
		else
			goHelpLast.SetActive (true);
	}

	void Update()
	{
		if (fInputQueueTimer > 0)
			fInputQueueTimer -= Time.deltaTime;

		if ( fInputQueueTimer < 0 )
		{
			fInputQueueTimer = 0f;
			bShouldWait = false;
		}

		if (canRestart)
		{
			if ( KSInputManager.Instance.isTouchDown () || Input.GetButton("Jump") )
			{
				RespawnPlayer ();
			}
			return;

		}
		// The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
		grounded = Physics2D.Linecast(transform.position, groundCheck.transform.position, 1 << LayerMask.NameToLayer("Ground"));  
		/*bool collidedWithWall = Physics2D.Linecast(transform.position, frontCheck.transform.position, 1 << LayerMask.NameToLayer("Ground"));  

		if (collidedWithWall) 
		{
			isDying = true;
			rigidbody2D.velocity = Vector2.zero;
		}
		*/
		// If the jump button is pressed and the player is grounded then the player should jump.

		if ( !shouldStartGame )
		{
			if(KSInputManager.Instance.isTouchDown () || Input.GetButton("Jump"))
			{
				rigidbody2D.velocity = new Vector2 (playerSpeed, rigidbody2D.velocity.y);
				shouldStartGame = true;
				Scene_Replay_AGAIN ();
			}
		}

		else 
		{

			if((KSInputManager.Instance.isTouchDown () || Input.GetButton("Jump")) && grounded)
			{
				if ( rigidbody2D.velocity.y > 0 )
					return;

				jump = true;
			}
			//else 
			//	bShouldWait = true;
		}
	}

	#region Game Logic


	public void IncreaseScore ()
	{
		++nGameScore;
		//labelScore.text = nGameScore + " / " + nTotalScore;
		//labelScore.Commit ();

		labelScore.UpdateLabel (nGameScore + " / " + nTotalScore);

		KSAudioManager.Instance.PlaySFX (audio_coin);
	}

	public void ResetScore ()
	{
		nGameScore = 0;
		//labelScore.text = nGameScore + " / " + nTotalScore;
		//labelScore.Commit ();
		labelScore.UpdateLabel (nGameScore + " / " + nTotalScore);
	}

	public void RespawnPlayer ()
	{
		isDying = canRestart = false;
		transform.localPosition = Vector2.zero;
		rigidbody2D.isKinematic = false;

		Scene_Replay_AGAIN ();

		rigidbody2D.fixedAngle = true;
		transform.rotation = Quaternion.identity;

		shouldStartGame = false;

		for ( int i = 0; i < Coins.Length; ++i )
		{
			Coins[i].GetComponent<Coins> ().Reset ();
		}


		BJ_CameraController2D_OG.Instance.bShouldFollow = true;
		
		ResetScore ();

		WinOrLose.Instance.Reset ();

	}

	public void KillPlayer ()
	{
		if (isDying || canRestart)
			return;

		BJ_CameraController2D_OG.Instance.bShouldFollow = false;

		//Debug.Log ("KillPlayer");
		rigidbody2D.velocity = Vector2.zero;
		//rigidbody2D.isKinematic = true;

		this.rigidbody2D.fixedAngle = false;
		this.rigidbody2D.angularVelocity = 375f;

		StartCoroutine( Enum_EnableRestart (0.3f));

		KSAudioManager.Instance.PlaySFX ( audio_die );

		KSDBManager.Instance.setInt (KSDBKeys.MYST_GAMESPLAYED, KSDBManager.Instance.getInt (KSDBKeys.MYST_GAMESPLAYED, 0) + 1); //inc
		labelGamesPlayed.UpdateLabel ("Attempts at Love: " + KSDBManager.Instance.getInt (KSDBKeys.MYST_GAMESPLAYED, 0));
	}

	IEnumerator Enum_EnableRestart ( float delay )
	{
		isDying = true;
		yield return new WaitForSeconds (delay);
		canRestart = true;
	}

	void OnCollisionEnter2D ( Collision2D other )
	{

		if ( other.collider.tag == "Finish" )
		{
			KillPlayer ();
		}
		else if ( other.collider.tag == "End2") 
		{
			WinOrLose.Instance.PlayWinOrLose ( nGameScore == nTotalScore );
			Scene_Win ( nGameScore == nTotalScore );
			StartCoroutine( Enum_EnableRestart (2f));
		}
		else if ( other.transform.tag == "Coin" && other.transform.GetComponent<Coins> ().isDummySpike && !jump )
		{
			if ( other.transform.GetComponent<Coins> ().hasBeenIgnored )
				return;

			other.transform.GetComponent<Coins> ().Collected ();
			other.transform.GetComponent<Coins> ().Ignore ();
			IncreaseScore ();
		}


		/*else if ( other.collider.tag == "Coin" )
		{
			other.transform.GetComponent<Coins> ().Collected ();
			IncreaseScore ();
		}*/

	}


	void OnTriggerEnter2D ( Collider2D other )
	{
		/*
		if ( other.tag == "Finish" )
		{
			KillPlayer ();
		}
		else if ( other.tag == "End2") 
		{
			KillPlayer ();
		}
		*/
	}


	public override void TriggerEnter ( RangeTrigger trigger )
	{

		if ( trigger.trigger_name == "front" )
		{
			if ( trigger.other.transform.GetComponent<SpriteRenderer>().enabled )
			{
				if ( trigger.other.transform.tag == "Coin" && trigger.other.transform.GetComponent<Coins> ().isDummySpike && !jump )
				{

					if ( trigger.other.transform.GetComponent<Coins> ().hasBeenIgnored )
						return;

					trigger.other.transform.GetComponent<Coins> ().Collected ();
					trigger.other.transform.GetComponent<Coins> ().Ignore ();
					IncreaseScore ();
				}
				else if ( trigger.other.transform.tag == "End2" )
				{
					WinOrLose.Instance.PlayWinOrLose ( nGameScore == nTotalScore );
					StartCoroutine( Enum_EnableRestart (2f));
					Scene_Win ( nGameScore == nTotalScore );

				}
				else
					KillPlayer ();
			}
			else 
			{
				if ( trigger.other.transform.GetComponent<Coins> () != null )
					trigger.other.transform.GetComponent<Coins> ().Ignore ();
			}
		}
		else if ( trigger.trigger_name == "head" )
		{
			if ( trigger.other.collider.tag == "Coin" )
			{

				if ( trigger.other.transform.GetComponent<Coins> ().hasBeenIgnored )
					return;

				trigger.other.transform.GetComponent<Coins> ().Collected ();
				trigger.other.transform.GetComponent<Coins> ().Ignore ();
				IncreaseScore ();
			}

			//Debug.Break ();
		}

		Debug.Log ("trigger name: " + trigger.trigger_name);
		
	}
	public override void TriggerStay ( RangeTrigger trigger )
	{
		
	}
	public override void TriggerExit ( RangeTrigger trigger )
	{
		
	}

	#endregion
	


	public float playerSpeed = 3f;

	bool delayOneFrame = false;

	void FixedUpdate ()
	{
		if ( delayOneFrame )
		{
			delayOneFrame = false;
			return;
		}

		//if(playerSpeed * rigidbody2D.velocity.x < maxSpeed)
			// ... add a force to the player.
		//rigidbody2D.AddForce(Vector2.right * playerSpeed * moveForce);

		if ( shouldStartGame && !isDying && rigidbody2D.velocity.x < playerSpeed)
			rigidbody2D.velocity = new Vector2 (playerSpeed, rigidbody2D.velocity.y);

		if(jump )//|| bShouldWait)
		{
			// Set the Jump animator trigger parameter.
			//anim.SetTrigger("Jump");
			
			// Play a random jump audio clip.
			//int i = Random.Range(0, jumpClips.Length);
			//AudioSource.PlayClipAtPoint(jumpClips[i], transform.position);
			
			// Add a vertical force to the player.
			rigidbody2D.AddForce(new Vector2(0f, jumpForce));
			
			// Make sure the player can't jump again until the jump conditions from Update are satisfied.
			jump = bShouldWait = false;
			delayOneFrame = true;
			KSAudioManager.Instance.PlaySFX ( audio_jump );

		}

		//transform.rotation = Quaternion.identity;

		return; //platformer code

		// Cache the horizontal input.
		float h = Input.GetAxis("Horizontal");
		
		// The Speed animator parameter is set to the absolute value of the horizontal input.
		//anim.SetFloat("Speed", Mathf.Abs(h));
		
		// If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
		if(h * rigidbody2D.velocity.x < maxSpeed)
			// ... add a force to the player.
			rigidbody2D.AddForce(Vector2.right * h * moveForce);
		
		// If the player's horizontal velocity is greater than the maxSpeed...
		if(Mathf.Abs(rigidbody2D.velocity.x) > maxSpeed)
			// ... set the player's velocity to the maxSpeed in the x axis.
			rigidbody2D.velocity = new Vector2(Mathf.Sign(rigidbody2D.velocity.x) * maxSpeed, rigidbody2D.velocity.y);
		
		// If the input is moving the player right and the player is facing left...
		if(h > 0 && !facingRight)
			// ... flip the player.
			Flip();
		// Otherwise if the input is moving the player left and the player is facing right...
		else if(h < 0 && facingRight)
			// ... flip the player.
			Flip();
		
		// If the player should jump...
		if(jump)
		{
			// Set the Jump animator trigger parameter.
			//anim.SetTrigger("Jump");
			
			// Play a random jump audio clip.
			//int i = Random.Range(0, jumpClips.Length);
			//AudioSource.PlayClipAtPoint(jumpClips[i], transform.position);
			
			// Add a vertical force to the player.
			rigidbody2D.AddForce(new Vector2(0f, jumpForce));
			
			// Make sure the player can't jump again until the jump conditions from Update are satisfied.
			jump = false;
		}
	}
	
	
	void Flip ()
	{
		// Switch the way the player is labelled as facing.
		facingRight = !facingRight;
		
		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
