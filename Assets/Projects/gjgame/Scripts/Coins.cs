﻿using UnityEngine;
using System.Collections;

public class Coins : MonoBehaviour {
	
	public bool isHidden = false;
	public bool isDummySpike = false;
	public bool isHint = false;

	public Sprite spriteHint;

	public Sprite original;

	public Sprite spriteGreenHit;
	public Sprite spriteYellowHit;

	public GameObject goHeart;

	public int hackIgnore = 0;

	public bool hasBeenIgnored = false;

	// Use this for initialization
	void Start () {
	
		original = GetComponent<SpriteRenderer> ().sprite;

		if (isHidden)
			GetComponent<SpriteRenderer> ().enabled = false;	
		if ( isHint )
			GetComponent<SpriteRenderer> ().sprite = spriteHint;

		//goHeart = GameObject.Find ("goHeart");
	}
	
	// Update is called once per frame
	void Update () {
		if ( hackIgnore > 0 )
			hackIgnore--;
	}

	public void Collected ()
	{
		if (isHidden)
		{
			GetComponent<SpriteRenderer> ().sprite = spriteGreenHit;
			GetComponent<SpriteRenderer> ().enabled = true;
		}
		else
		{
			//Debug.Break ();
			GetComponent<SpriteRenderer> ().sprite = spriteYellowHit;
		}
		BounceBlock1 ();
		DisplayHeart ( transform.position );
	}

	public void Ignore ()
	{
		hasBeenIgnored = true;
		//if ( !isHidden )
			StartCoroutine (delayIgnore ());
	}

	IEnumerator delayIgnore ()
	{
		yield return new WaitForSeconds (0.0001f);
		collider2D.enabled = false;
	}

	public void DisplayHeart ( Vector3 pos )
	{
		goHeart.GetComponent<SpriteRenderer> ().enabled = true;
		goHeart.GetComponent<SpriteRenderer> ().color = Color.white;
		//labelScoreDisplay.GetComponent<OLabel> ().UpdateLabel( BJ_LevelManager.Instance.nGameScore );
		goHeart.transform.position = pos + new Vector3(0,1,0);
		//Color color = goHeart.renderer.material.color;
		//color.a = 1.0f;
		//goHeart.renderer.material.color = color;
		//LeanTween.scale ( labelScoreDisplay.gameObject, new Vector2 ( 0.125f, 0.125f ), 0.85f);
		LeanTween.moveY(goHeart.gameObject, goHeart.transform.position.y + 2.25f, 1.25f);
		LeanTween.alpha(goHeart.gameObject, 0, 1.25f).setOnComplete(HideHeart);
		
		//ODBMgr.Instance.setInt (ODBKeys.GAME_COINS, ODBMgr.Instance.getInt (ODBKeys.GAME_COINS) + (nGameScore + 1));
		//labelCoinTotal.UpdateLabel ( ODBMgr.Instance.getInt ( ODBKeys.GAME_COINS ) );
		//Debug.Break ();
	}

	public void HideHeart ()
	{
		goHeart.GetComponent<SpriteRenderer> ().enabled = false;
	}

	public void BounceBlock1 ()
	{
		Debug.Log (transform.position);
		LeanTween.moveY (gameObject, transform.position.y+0.35f, 0.145f).setOnComplete ( BounceBlock2 ).tweenType = LeanTweenType.easeInOutQuad;
	}
	
	private void BounceBlock2 ( )
	{
		LeanTween.moveY (gameObject, transform.position.y-0.35f, 0.145f).tweenType = LeanTweenType.easeInOutQuad;


	}	

	public void Reset ()
	{
		collider2D.enabled = true;
		hasBeenIgnored = false;
		GetComponent<SpriteRenderer> ().sprite = original;
		if (isHidden)
			GetComponent<SpriteRenderer> ().enabled = false;
	}
}
