﻿using UnityEngine;
using System.Collections;

	[System.Serializable]
	public class CameraParameters
	{
		public bool bShouldOffsetX = true;
		public bool bShouldOffsetY = false;

		public bool bShouldIgnoreYLower = true;
		public bool bShouldIgnoreXLower = true;

		public float CameraYOffset = 4.0f;
		public float CameraXOffset = 0.0f;
	}


	public class BJ_CameraController2D_OG : Singleton<BJ_CameraController2D_OG> {

		#region Parameters

		public CameraParameters Parameters;

		public bool bShouldFollow = true;

		#endregion

		#region Declarations

		#endregion

		#region Game Loop
	
		void OnDrawGizmos ()
		{

		}

		// Use this for initialization
		void Start () {

		}
		
		// Update is called once per frame
		void Update () {


		}

		public Transform m_Player;

		void LateUpdate ()
		{

		if (!bShouldFollow)
			return;

		transform.position = new Vector3 (m_Player.position.x + Parameters.CameraXOffset, transform.position.y + Parameters.CameraYOffset, transform.position.z);


		return;

			Vector3 newPos = transform.position;

			//if (BJ_PlayerController.Instance.obPlayer.isCurrentlyDying)
			//	return;

			if ( Parameters.bShouldOffsetY && !Parameters.bShouldOffsetX ) //offset y 
			{

			float newY = m_Player.position.y + Parameters.CameraYOffset;
			float newX = m_Player.position.x + Parameters.CameraXOffset;

				if ( Parameters.bShouldIgnoreYLower && newY < transform.position.y )
					newY = transform.position.y;

				if ( Parameters.bShouldIgnoreXLower && newX < transform.position.x )
					newX = transform.position.x;

				newPos = new Vector3 ( transform.position.x, newY, -10f );
			}
			else if ( !Parameters.bShouldOffsetY && Parameters.bShouldOffsetX ) //offset x
				newPos = new Vector3 ( m_Player.position.x + Parameters.CameraXOffset, transform.position.y, -10f );
			else if ( Parameters.bShouldOffsetY && Parameters.bShouldOffsetX ) //offset both
			{
					
				float newY = m_Player.position.y + Parameters.CameraYOffset;
				
				if ( Parameters.bShouldIgnoreYLower && newY < transform.position.y )
					newY = transform.position.y;

				newPos = new Vector3 ( m_Player.position.x + Parameters.CameraXOffset, newY, -10f );
			}

			transform.position = newPos;
		}

		#endregion
	}
