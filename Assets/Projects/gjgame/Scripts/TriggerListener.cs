﻿using UnityEngine;
using System.Collections;

abstract public class TriggerListener : MonoBehaviour {
	public abstract void TriggerEnter ( RangeTrigger trigger );
	public abstract void TriggerStay ( RangeTrigger trigger );
	public abstract void TriggerExit ( RangeTrigger trigger );

}
