﻿using UnityEngine;
using System.Collections;

public class RangeTrigger : MonoBehaviour {

	public string trigger_name;
	public GameObject trigger_receiver;
	public Collision2D other;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	
	void OnCollisionEnter2D ( Collision2D other )
	{
		this.other = other;
		trigger_receiver.SendMessage ( "TriggerEnter", this, SendMessageOptions.RequireReceiver );
	}

	/*
	void OnTriggerEnter ( Collider other )
	{
		//trigger_listeners.gameObject.sen
		//trigger_listeners.TriggerEnter ( trigger_name, other );
		this.other = other;
		trigger_receiver.SendMessage ( "TriggerEnter", this, SendMessageOptions.RequireReceiver );
	}

	void OnTriggerStay ( Collider other )
	{
		//trigger_listeners.TriggerStay ( trigger_name, other );
		this.other = other;
		trigger_receiver.SendMessage ( "TriggerStay", this, SendMessageOptions.RequireReceiver );
	}

	void OnTriggerExit ( Collider other )
	{
		//trigger_listeners.TriggerExit ( trigger_name, other );
		this.other = other;
		trigger_receiver.SendMessage ( "TriggerExit", this, SendMessageOptions.RequireReceiver );
	}
	*/
}
