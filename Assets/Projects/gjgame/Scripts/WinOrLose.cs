﻿using UnityEngine;
using System.Collections;

public class WinOrLose : Singleton<WinOrLose> {

	public GameObject goWin;
	public GameObject goLose;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlayWinOrLose ( bool win )
	{
		goWin.SetActive ( win );
		goLose.SetActive ( !win );
	}

	public void Reset ()
	{
		goWin.SetActive ( false );
		goLose.SetActive ( false );
	}
}
