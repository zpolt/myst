﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	#region Declaration

	public float PLAYER_LEFT_DEG = 0f;
	public float PLAYER_RIGHT_DEG = 180f;
	public float PLAYER_JUMP_DEG = 90f;

	public float PLAYER_LEFT_POW = 2f;
	public float PLAYER_RIGHT_POW = 2f;
	public float PLAYER_JUMP_POW = 5f;

	#endregion

	#region Game Logic -- private

	public Vector2 GetMovementForce ( bool left )
	{
		Vector2 v = Vector2.zero;

		if ( left )
		{
			v = new Vector2 (Mathf.Cos (Mathf.Deg2Rad * PLAYER_LEFT_DEG), 
                 Mathf.Sin (Mathf.Deg2Rad * PLAYER_LEFT_DEG)) * PLAYER_LEFT_POW;
		}
		else
		{
			v = new Vector2 (Mathf.Cos (Mathf.Deg2Rad * PLAYER_RIGHT_DEG), 
                 Mathf.Sin (Mathf.Deg2Rad * PLAYER_RIGHT_DEG)) * PLAYER_RIGHT_POW;
		}

		return v;
	}

	public Vector2 GetJumpForce ( )
	{
		Vector2 v = new Vector2 (Mathf.Cos (Mathf.Deg2Rad * PLAYER_JUMP_DEG), 
                 Mathf.Sin (Mathf.Deg2Rad * PLAYER_JUMP_DEG)) * PLAYER_JUMP_POW;

		return v;
	}


	#endregion

	#region Game Loop

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if ( Input.anyKeyDown )
		{
			if ( Input.GetKeyDown ( KeyCode.A )) 
			{
				rigidbody2D.AddForce ( GetMovementForce ( true ) );
			}

			else if ( Input.GetKeyDown ( KeyCode.D )) 
			{
				rigidbody2D.AddForce ( GetMovementForce ( false ) );
			}

			else if ( Input.GetKeyDown ( KeyCode.W )) 
			{
				rigidbody2D.AddForce ( GetJumpForce () );
			}
		}

	}

	#endregion
}
